/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.AbstractDistributionInstaller
import org.ysb33r.grolifant.compatibility.testing.helpers.ExecHelperSpecification
import org.ysb33r.grolifant.api.exec.AbstractCommandExecSpec
import org.ysb33r.grolifant.api.exec.AbstractExecWrapperTask
import org.ysb33r.grolifant.api.exec.ExternalExecutable
import org.ysb33r.grolifant.api.exec.ResolveExecutableByVersion
import org.ysb33r.grolifant.api.OperatingSystem
import org.ysb33r.grolifant.api.exec.AbstractToolExtension

class AbstractExecWrapperTaskSpec extends ExecHelperSpecification {

    final static String DISTVER =  ExecHelperSpecification.DISTVER

    Project project = ProjectBuilder.builder().build()

    def 'Run a command-line tool as a task'() {
        setup:
        project.allprojects {
            apply plugin : MyPlugin

            // tag::configure-the-tool[]
            toolConfig {
                executable path : '/usr/local/bin/mycmd' // <1>
            }

            mycmd {
                toolConfig {
                    executable path : '/opt/local/bin/mycmd' // <2>
                }
            }
            // end::configure-the-tool[]
        }

        // The reason the value is set again is because the previous two are
        // merely for coverage and documentation purposes.
        final String testScriptPath = scriptToPass.absolutePath
        project.allprojects {
            mycmd {
                toolConfig {
                    executable path : testScriptPath
                }
            }
        }

        when:
        project.evaluate()
        project.tasks.mycmd.exec()

        then:
        noExceptionThrown()
    }

    def 'Add a version resolver'() {
        setup:
        project.allprojects {
            apply plugin : MyPlugin

            // tag::configure-the-version[]
            toolConfig {
                executable version : '0.2'
            }
            // end::configure-the-version[]
        }

        // The reason the value is set again is because the previous two are
        // merely for coverage and documentation purposes.
        final String testVersion = DISTVER
        project.allprojects {
            toolConfig {
                executable version : testVersion
            }
        }

        when:
        project.evaluate()
        project.tasks.mycmd.exec()

        then:
        noExceptionThrown()
    }

    static
    // tag::example-execspec[]
    class MyCmdExecSpec extends AbstractCommandExecSpec {
        MyCmdExecSpec(Project project, ExternalExecutable registry) {
            super(project,registry)
        }
    }
    // end::example-execspec[]

    static
    // tag::example-extension-with-version[]
    // tag::example-extension[]
    class MyExtension extends AbstractToolExtension { // <1>

        static final String NAME = 'toolConfig'

        MyExtension(Project project) { // <2>
            super(project)
            // end::example-extension[]
            addVersionResolver(project)
            // tag::example-extension[]
        }

        MyExtension(Task task) {
            super(task,NAME) // <3>
            // end::example-extension[]
            addVersionResolver(task.project)
            // tag::example-extension[]
        }

        // end::example-extension[]
        private void addVersionResolver(Project project) {

            ResolveExecutableByVersion.DownloaderFactory downloaderFactory = { // <4>
                Map<String, Object> options, String version,Project p -> // <5>
                    new MyInstaller(version,p)
                } as ResolveExecutableByVersion.DownloaderFactory

            ResolveExecutableByVersion.DownloadedExecutable resolver = { MyInstaller installer -> // <6>
                    new File(installer.getDistributionRoot(), // <7>
                        OperatingSystem.current().windows ? 'test.bat' : 'test.sh'
                    )
                } as ResolveExecutableByVersion.DownloadedExecutable

            getResolverFactoryRegistry().registerExecutableKeyActions(
                new ResolveExecutableByVersion(project,downloaderFactory,resolver) // <8>
            )
        }
        // tag::example-extension[]
    }
    // end::example-extension[]
    // end::example-extension-with-version[]

    static
    // tag::example-task[]
    class MyWrapperTask extends AbstractExecWrapperTask<MyCmdExecSpec,MyExtension> { // <1>

        MyWrapperTask() {
            super()
            myExtension = extensions.create(MyExtension.NAME,MyExtension,this) // <2>
        }

        @Override
        protected MyCmdExecSpec createExecSpec() { // <3>
            new MyCmdExecSpec(project,getToolExtension().getResolver())
        }

        @Override
        protected MyCmdExecSpec configureExecSpec(MyCmdExecSpec execSpec) { // <4>
            execSpec.cmdArgs '--yellow', '--bright'
            execSpec // <5>
        }


        @Override
        protected MyExtension getToolExtension() { // <6>
            this.myExtension
        }

        private MyExtension myExtension
    }
    // end::example-task[]

    static
    // tag::example-plugin[]
    class MyPlugin implements Plugin<Project> {
        void apply(Project project) {
            project.extensions.create(MyExtension.NAME,MyExtension,project) // <1>
            project.tasks.create('mycmd',MyWrapperTask) // <2>
        }
    }
    // end::example-plugin[]


    static
    // tag::example-downloader[]
    class MyInstaller extends AbstractDistributionInstaller {


        MyInstaller(final String version,Project project) {
            super('Test Distribution',version,DISTPATH,project)
            addExecPattern('**/*.sh','**/*.bat')
        }
        // end::example-downloader[]

        @Override
        URI uriFromVersion(String version) { // <2>
            TESTDIST_DIR.toURI().resolve("testdist-${DISTVER}.zip")
        }

        private static final File TESTDIST_DIR = ExecHelperSpecification.TESTDIST_DIR
        private static final String DISTPATH = 'foo/bar'
        // tag::example-downloader[]
    }
    // end::example-downloader[]

}