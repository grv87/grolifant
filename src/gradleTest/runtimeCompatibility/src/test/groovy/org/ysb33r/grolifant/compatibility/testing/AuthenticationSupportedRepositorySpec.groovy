/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.AuthenticationContainer
import org.gradle.api.artifacts.repositories.PasswordCredentials
import org.gradle.api.credentials.Credentials
import org.gradle.authentication.Authentication
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.repositories.AuthenticationSupportedRepository
import org.ysb33r.grolifant.api.repositories.SimplePasswordCredentials
import spock.lang.Specification

class AuthenticationSupportedRepositorySpec extends Specification {

    Project project = ProjectBuilder.builder().build()
    AuthenticationSupportedRepository repository

    void setup() {
        repository = makeRepo()
    }

    void 'Set repo name'() {
        when:
        repository.name = 'foo'

        then:
        repository.name == 'foo'
    }

    void 'If credentials have not been defined, simple credentials are used'() {
        expect:
        repository.credentials != null
        repository.credentials instanceof SimplePasswordCredentials
    }

    void 'Configure credentials'() {
        when:
        repository.credentials( { PasswordCredentials pw ->
            pw.with {
                username = 'foo'
                password = 'bar'
            }
        } as Action<PasswordCredentials>)

        then:
        repository.credentials.username == 'foo'
        repository.credentials.password == 'bar'
    }

    void 'Get credentials for a different type of class'() {
        when:
        Credentials creds = repository.getCredentials(AltCredentials)

        then:
        creds instanceof AltCredentials

        when:
        repository.credentials(AltCredentials,{ AltCredentials ac ->
            ac.token = '123'
        } as Action<AltCredentials>)

        then:
        creds.token == '123'
    }

    void 'Cannot attempt a different type of credentials when another type is already initialised'() {
        when:
        PasswordCredentials pw = repository.credentials

        then:
        pw != null

        when:
        repository.getCredentials(AltCredentials)

        then:
        thrown(IllegalArgumentException)
    }

    void 'Authentication container is not null'() {
        expect:
        repository.authentication != null
    }

    void 'Configure authentication container'() {
        when:
        repository.authentication({   AuthenticationContainer ac ->
            ac.add( new Authentication() {
                @Override
                String getName() {
                    'foobar'
                }
            })
        } as Action<AuthenticationContainer>)

        then:
        repository.authentication.getByName('foobar')
    }

    AuthenticationSupportedRepository makeRepo() {
        new FakeRepo(project)
    }

    static class FakeRepo extends AuthenticationSupportedRepository {
        FakeRepo(Project project) {
            super(project)
        }

        void content(Action action) {}
    }

    static class AltCredentials implements Credentials {
        String token
    }
}