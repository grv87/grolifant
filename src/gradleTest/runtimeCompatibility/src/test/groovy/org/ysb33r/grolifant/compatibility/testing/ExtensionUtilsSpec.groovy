/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

import static org.ysb33r.grolifant.api.ExtensionUtils.bindDependencyHandlerService
import static org.ysb33r.grolifant.api.ExtensionUtils.bindRepositoryHandlerService

class ExtensionUtilsSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    void 'Add a repository handler extension by object'() {
        given:
        def byObject = new RepositoryExtension()

        bindRepositoryHandlerService(project, 'byObject', byObject)

        when:
        project.allprojects {
            repositories {
                byObject.foo()
            }
        }

        then:
        noExceptionThrown()
    }

    void 'Add a repository handler extension by class'() {
        given:
        bindRepositoryHandlerService(project, 'byClass', RepositoryExtension)

        when:
        project.allprojects {
            repositories {
                byClass.foo()
            }
        }

        then:
        noExceptionThrown()
    }

    void 'Add a dependency handler extension by object'() {
        given:
        def byObject = new DependencyExtension()

        bindDependencyHandlerService(project, 'byObject', byObject)

        when:
        project.allprojects {
            dependencies {
                byObject {

                }
            }
        }

        then:
        noExceptionThrown()
    }

    void 'Add a dependency handler extension by class'() {
        given:
        bindDependencyHandlerService(project, 'byClass', DependencyExtension)

        when:
        project.allprojects {
            dependencies {
                byClass {

                }
            }
        }

        then:
        noExceptionThrown()
    }

    static class RepositoryExtension {

        RepositoryExtension() {}

        RepositoryExtension(Project h) {}

        void foo() {}
    }

    static class DependencyExtension {

        DependencyExtension() {}

        DependencyExtension(Project h) {}

        void call(Closure cfg) {

        }
    }
}