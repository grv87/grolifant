/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing.helpers

import org.ysb33r.grolifant.api.OperatingSystem
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.PosixFilePermission

class ExecHelperSpecification extends Specification {
    static final File TESTDIST_DIR = new File(System.getProperty('COMPAT_TEST_RESOURCES_DIR') ?: 'src/gradleTest/runtimeCompatibility/src/test/resources').absoluteFile
    static final String DISTVER  = '0.2'

    static final String toolExt = OperatingSystem.current().windows ? 'cmd' : 'sh'
    static final File scriptToPass = new File(TESTDIST_DIR,'mycmd.' + toolExt)

    void setupSpec() {
        // This code is here to work around the case that execute permissions are lost when GradleTest copies files.
        if(!OperatingSystem.current().windows) {
            Path scriptPath = scriptToPass.toPath()
            Set perms = Files.getPosixFilePermissions(scriptPath)
            perms.add(PosixFilePermission.OWNER_EXECUTE)
            Files.setPosixFilePermissions(scriptPath,perms)
        }
    }


}