/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing.git

import org.ysb33r.grolifant.api.git.CloudGitConfigurator
import org.ysb33r.grolifant.api.git.GitHubArchive
import org.ysb33r.grolifant.api.git.GitLabArchive
import spock.lang.Specification

class CloudGitSpec extends Specification {

    void 'GitLab archive must be described correctly'() {
        given:
        CloudGitConfigurator git = new GitLabArchive()

        when:
        git.repository = 'grolifant'
        git.organisation = 'ysb33rOrg'
        git.branch = 'master'

        then:
        git.archiveUri.toString() == 'https://gitlab.com/ysb33rOrg/grolifant/-/archive/master/grolifant-master.zip'

        when:
        git.tag = 'RELEASE_0_8'

        then:
        git.archiveUri.toString() == 'https://gitlab.com/ysb33rOrg/grolifant/-/archive/RELEASE_0_8/grolifant-RELEASE_0_8.zip'

        when:
        git.commit = 'd0cf5cd8b94a4a27f534d3b28b015b2c58b49bb0'

        then:
        git.archiveUri.toString() == 'https://gitlab.com/ysb33rOrg/grolifant/-/archive/d0cf5cd8b94a4a27f534d3b28b015b2c58b49bb0/grolifant-d0cf5cd8b94a4a27f534d3b28b015b2c58b49bb0.zip'

    }

    void 'GitHub archive must be described correctly'() {
        given:
        CloudGitConfigurator git = new GitHubArchive()

        when:
        git.repository = 'grolifant'
        git.organisation = 'ysb33rOrg'
        git.branch = 'master'

        then:
        git.archiveUri.toString() == 'https://github.com/ysb33rOrg/grolifant/archive/master.zip'

        when:
        git.tag = 'RELEASE_0_8'

        then:
        git.archiveUri.toString() == 'https://github.com/ysb33rOrg/grolifant/archive/RELEASE_0_8.zip'

        when:
        git.commit = 'd0cf5cd8b94a4a27f534d3b28b015b2c58b49bb0'

        then:
        git.archiveUri.toString() == 'https://github.com/ysb33rOrg/grolifant/archive/d0cf5cd8b94a4a27f534d3b28b015b2c58b49bb0.zip'

    }

}