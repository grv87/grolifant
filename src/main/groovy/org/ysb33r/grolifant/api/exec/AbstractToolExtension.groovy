/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.api.exec

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.ysb33r.grolifant.api.AbstractCombinedProjectTaskExtension
import org.ysb33r.grolifant.api.errors.ExecConfigurationException

/** Use as a base class for extensions that will wrap tools.
 *
 * <p> This base class will also enable extensions to discover whether they are inside a task or a
 * project.
 *
 * @since 0.4
 */
@CompileStatic
abstract class AbstractToolExtension extends AbstractCombinedProjectTaskExtension {

    /** Set the parameters for location an exe.
     *
     * @param opts Options for locating the exe. The exact parameters are tool-specific.
     */
    void executable( final Map<String,?> opts ) {
        this.resolvableExecutable = getResolver().getResolvableExecutable((Map<String,Object>)opts)
    }

    /** Obtain a lazy-evaluated object to resolve a path to an exe.
     *
     * @return An object for which the {@link exe()} method will resolve a path on-demand.
     */
    ResolvableExecutable getResolvableExecutable() {
        ResolvableExecutable exe = this.resolvableExecutable
        if(exe == null && task != null) {
            exe = ((AbstractToolExtension)getProjectExtension()).getResolvableExecutable()
        }
        if(exe == null) {
            throw new ExecConfigurationException("Executable has not been configured")
        }
        return exe
    }

    /** Get access to object that can resolve an exe's location from a property map
     *
     * @return Resolver
     */
    ExternalExecutable getResolver() {
        this.registry ?: ((AbstractToolExtension)getProjectExtension()).getResolver()
    }

    /** Attach this extension to a project
     *
     * @param project Project to attach to.
     */
    protected AbstractToolExtension(Project project) {
        super(project)
        this.registry = new ResolverFactoryRegistry(project)
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to.
     * @param projectExtName Name of the extension that is attached to the project.
     */
    protected AbstractToolExtension(Task task,final String projectExtName) {
        super(task,projectExtName)
    }

    /** Access to the registry of exe resolver factories.
     *
     * <p> This is used to add additional factory types i.e. for version-based resolving.
     *
     * @return Registry
     */
    protected ResolverFactoryRegistry getResolverFactoryRegistry() {
        this.registry ?: ((AbstractToolExtension)getProjectExtension()).getResolverFactoryRegistry()
    }

    private final ResolverFactoryRegistry registry
    private ResolvableExecutable resolvableExecutable


}
