/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.git.AbstractCloudGit
import org.ysb33r.grolifant.api.git.GitHubArchive
import org.ysb33r.grolifant.api.git.GitLabArchive
import org.ysb33r.grolifant.api.git.GitRepoArchiveDownloader
import spock.lang.Specification
import spock.lang.Unroll

class GitRepoArchiveDownloaderSpec extends Specification {
    static final File TESTDIST_DIR = new File(System.getProperty('TEST_RESOURCES_DIR') ?: '.', 'src/test/resources')

    Project project = ProjectBuilder.builder().build()

    @Unroll
    void 'Download an archive from #name'() {

        given:
        AbstractCloudGit git = type.newInstance()
        git.baseUri = new File(TESTDIST_DIR, "${name}").toURI()

        // tag::config-git-repo[]
        git.organisation = 'ysb33rOrg'
        git.repository = 'grolifant'
        git.branch = 'master' // <2>
        // end::config-git-repo[]

        // tag::config-git-downloader[]
        GitRepoArchiveDownloader downloader = new GitRepoArchiveDownloader(git, project) // <1>
        // end::config-git-downloader[]

        // tag::config-download-root[]
        downloader.downloadRoot = new File(project.buildDir,'my-archive') // <1>
        // end::config-download-root[]

        when:
        // tag::config-git-downloader[]
        File root = downloader.archiveRoot // <2>
        // end::config-git-downloader[]

        then:
        downloader.downloadRoot.exists()
        new File(root,'src/test/resources').exists()

        where:
        name     | type
        'github' | GitHubArchive
        'gitlab' | GitLabArchive
    }
}