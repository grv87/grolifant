/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant

import org.ysb33r.grolifant.api.FileUtils
import spock.lang.Specification
import spock.lang.Unroll

class FileUtilsSpec extends Specification {

    @Unroll
    void 'Safe file name: #name'() {

        expect:
        safe == FileUtils.toSafeFileName(name)

        where:
        name         || safe
        'abc'        || 'abc'
        'a.bc_d-e$f' || 'a.bc_d-e$f'
        'a@b!c&e'    || 'a#40!b#21!c#26!e'
    }

    @Unroll
    void 'Find class location for #clazz'() {
        expect:
        FileUtils.resolveClassLocation(clazz) != null

        where:
        clazz << [File, FileUtilsSpec, FileUtils]
    }
}